package com.kuei;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyCount
 */
public class MyCount extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    int count =0;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyCount() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init() throws ServletException {  
    	System.out.println("MyCount init....");
    	System.out.println(getInitParameter("initial"));
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setHeader("Pragma", "No-cache"); // HTTP 1.0
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0); // proxies
		PrintWriter out = response.getWriter();

		out.println("<HTML>");
		out.println("<HEAD><TITLE>Hello World</TITLE></HEAD>");
		out.println("<BODY>");
		out.println("<BIG>Hello World , 世界你好 !</BIG>"+count++);
		out.println("</BODY></HTML>");
		response.sendError(response.SC_NOT_FOUND, "抱改鴨鴨");
	}

}
